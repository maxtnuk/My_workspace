# project_test

TODO: Write a description here

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  project_test:
    github: [your-github-name]/project_test
```

## Usage

```crystal
require "project_test"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it ( https://github.com/[your-github-name]/project_test/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create a new Pull Request

## Contributors

- [[your-github-name]](https://github.com/[your-github-name]) maxtnuk - creator, maintainer
